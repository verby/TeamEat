-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 02 Février 2017 à 16:12
-- Version du serveur :  10.1.21-MariaDB
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `project`
--

-- -------------------------------------------------------

--
-- Structure de la table `assocrestomenu`
--

DROP TABLE IF EXISTS commande;
DROP TABLE IF EXISTS avis; 
DROP TABLE IF EXISTS menu;
DROP TABLE IF EXISTS restaurant;
DROP TABLE IF EXISTS utilisateur;

-- --------------------------------------------------------

--
-- Structure de la table `avis`
--

CREATE TABLE `avis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idRestaurant` int(11) NOT NULL,
  `idUtilisateur` int(11) NOT NULL,
  `dateAvis` timestamp NOT NULL,
  `commentaire` varchar(256) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idMenu` int(11) NOT NULL,
  `idUtilisateur` int(11) NOT NULL,
  `idParent` int(11),
  `conducteur` tinyint(1),
  `accompagne` tinyint(1),
  `nom` varchar(32) COLLATE utf8_bin NOT NULL,
  `dateCommande` datetime NOT NULL,
  `quantite` int(3),
  `statut` varchar(32) COLLATE utf8_bin NOT NULL,
  `commentaire` varchar(248) COLLATE utf8_bin,
  `paye` tinyint(1),
  PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idRestaurant` int(11) NOT NULL,
  `nom` varchar(104) COLLATE utf8_bin NOT NULL,
  `prix` decimal(10,2) NOT NULL,
  `description` varchar(256) COLLATE utf8_bin,
  `image` varchar(256) COLLATE utf8_bin,  
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `restaurant`
--

CREATE TABLE `restaurant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(32) COLLATE utf8_bin NOT NULL,
  `adresse` varchar(56) COLLATE utf8_bin NOT NULL,
  `telephone` varchar(16) COLLATE utf8_bin NOT NULL,
  `horaires` varchar(16) COLLATE utf8_bin NOT NULL,
  `statut` varchar(32) NOT NULL,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(56) COLLATE utf8_bin NOT NULL,
  `prenom` varchar(56) COLLATE utf8_bin NOT NULL,
  `mail` varchar(64) COLLATE utf8_bin NOT NULL,
  `motdepasse` varchar(256) COLLATE utf8_bin NOT NULL,
  `vehicule` tinyint(1) NOT NULL,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



--
-- Contraintes pour la table `menu`
--
ALTER TABLE `menu`
   ADD CONSTRAINT `menu_ibfk_2` FOREIGN KEY (`idRestaurant`) REFERENCES `restaurant` (`id`);
  

--
-- Contraintes pour la table `avis`
--
ALTER TABLE `avis`
 ADD CONSTRAINT `avis_ibfk_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`id`),
 ADD CONSTRAINT `avis_ibfk_2` FOREIGN KEY (`idRestaurant`) REFERENCES `restaurant` (`id`);

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `commande_ibfk_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`id`),
  ADD CONSTRAINT `commande_ibfk_2` FOREIGN KEY (`idMenu`) REFERENCES `menu` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
