import { Component,OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {Location} from '@angular/common';
import { Observable }     from 'rxjs/Observable';
import {Commande} from '../models/commande';
import {Restaurant} from '../models/restaurant';
import {Menu} from '../models/menu';
import {CommandeService} from '../services/commande.service'
import {RestaurantService} from '../services/restaurant.service'
import {MenuService} from '../services/menu.service'


declare var $: any
@Component({
    selector:'commandes',
    templateUrl: './commandes.component.html',
    styleUrls: [ './commandes.component.css' ]
  })

  export class CommandesComponent implements OnInit {

      commandes: Observable<Commande[]>;
      restaurants: Observable<Restaurant[]>;
      menus : Observable <Menu[]>;
      selectedCommande: Commande;
      commande:FormGroup;
      res: Object;

      constructor(
        private route: ActivatedRoute,
        private router: Router,
        private commandeService : CommandeService,
        private restaurantService : RestaurantService,
        private menuService : MenuService,
        private location: Location,
        private fb: FormBuilder
      ) {}

      ngOnInit(): void {
        this.commande=this.fb.group({
          nom:  ['', [Validators.required, Validators.required]],
          dateCommande: ['', [Validators.required, Validators.minLength(2)]],
          conducteur:  [''],
          accompagne:  [''],
          commentaire: ['']
        })
        this.route.params
          .subscribe(params => {
          this.commandes = this.commandeService.getCommandes();
          this.restaurants = this.restaurantService.getRestaurants();
        });
      }


        onSubmit({ value, valid }: { value: Commande, valid: boolean }) {
        //Initialistion des variables idUtilisateurs et idMenu
            value.idMenu=1;
            value.idUtilisateur=+localStorage.getItem("utilisateur");
          this.ajouter(value);
        }

        openModal(commande: Commande): void {
          this.selectedCommande = commande;
       }

       openModalCreation(): void {
       }

       goBack(): void {
        this.location.back();
       }

       ajouter(commande:Commande): void {
        this.commandeService.create(commande)
            .subscribe(res  => this.commandes = this.commandeService.getCommandes());
        }

        supprimer(): void {
          this.commandeService
              .supprimer(this.selectedCommande.id)
              .subscribe(res  => this.commandes = this.commandeService.getCommandes());
        }


      }
