import { Component,OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Observable }     from 'rxjs/Observable';
import {Restaurant} from '../models/restaurant';
import {Menu} from '../models/menu';
import {Avis} from '../models/avis';
import {Utilisateur} from '../models/utilisateur';
import {RestaurantService} from '../services/restaurant.service';
import {UtilisateurService} from '../services/utilisateur.service';
import {AvisService} from '../services/avis.service';
import {MenuService} from '../services/menu.service';

import { AgmCoreModule } from 'angular2-google-maps/core';
import { MapsAPILoader } from 'angular2-google-maps/core';

declare var google: any;

@Component({
  selector:'restaurant-detail',
  templateUrl: './restaurant-detail.component.html',
  styleUrls: [ './restaurant-detail.component.css' ]
})

export class RestaurantDetailComponent implements OnInit {
  lat: number = 45.217805;
  lng: number = 5.812897;

  restaurant: Restaurant;
  utilisateur: Utilisateur;
  utilisateurs: Observable<Utilisateur[]>;
  menus: Observable<Menu[]>;
  selectedMenu: Menu;
  menu:FormGroup;
  avis:FormGroup;
  allAvis: Observable<Avis[]>;
  selectedAvis: Avis;
  res: Object;
  moyenne: Observable<number>;

  constructor(
    private route: ActivatedRoute,
    private restaurantService : RestaurantService,
    private utilisateurService :UtilisateurService,
    private menuService :MenuService,
    private avisService : AvisService,
    private fb: FormBuilder,
    private location: Location,
    private mapsAPILoader: MapsAPILoader
  ) {
//Google maps
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    let win=(window as any);
    if (win.location.search.split("loaded=")[1]){
      var lat = getParameterByName('lat', win.location.search);
      this.lat = +lat;
      var lng = getParameterByName('lng', win.location.search);
      this.lng = +lng;
    }
  }

  ngOnInit(): void {
    this.menu=this.fb.group({
      nom: ['', [Validators.required, Validators.minLength(2)]],
      description: ['', [Validators.required, Validators.minLength(2)]],
      prix: ['', [Validators.required, Validators.minLength(1)]]
    })

    this.avis=this.fb.group({
      idUtilisateur:[localStorage.getItem("utilisateur")],
      commentaire: ['', [Validators.required, Validators.minLength(2)]],
      note:['']
    })
    //Chargement du restaurant
    this.route.params
    .switchMap((params: Params) => this.restaurantService.getRestaurant(+params['id']))
    .subscribe(restaurant => this.restaurant = restaurant);
    //Chargement des avis
    this.route.params
    .subscribe(params => {
      this.allAvis = this.avisService.getAvisByIdRestaurant(+params['id']);


    });
    //Chargement des menus
      this.route.params
      .subscribe(params => {
        this.menus = this.menuService.getMenusByIdRestaurant(+params['id']);
      });


      //Moyenne
        this.route.params
        .subscribe(params => {
          this.moyenne = this.avisService.getNoteByIdRestaurant(+params['id']);
        });

      /*
      //Chargement google maps
      this.mapsAPILoader.load().then(() => {
        var geocoder = new google.maps.Geocoder();

        let address = this.restaurant.adresse;

        geocoder.geocode( { 'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            this.lat = results[0].geometry.location.lat();
            this.lng = results[0].geometry.location.lng();

            let win=(window as any);
            if (! win.location.search.split("loaded=")[1]){
              win.location.search='?loaded=1&lat=' + this.lat + '&lng=' + this.lng;
            }
          } else {
            console.log('Error - ', results, ' & Status - ', status);
          }
        });
      });
      */

    }

    //Menus
    onSubmit({ value, valid }: { value: Menu, valid: boolean }) {
      this.ajouter(value);
    }

    openModal(menu: Menu): void {
      this.selectedMenu = menu;
    }

    ajouter(menu:Menu): void {
      menu.idRestaurant = this.restaurant.id;
      this.menuService.create(menu)
      .subscribe(res => {
        this.menus = this.menuService.getMenusByIdRestaurant(+this.restaurant.id);
      });
    }

    save(): void {
      this.route.params
      .switchMap((params: Params) => this.restaurantService.update(this.restaurant))
      .subscribe(() => this.goBack());
    }
    supprimer(): void {
      this.menuService
      .supprimer(this.selectedMenu.id)
      .subscribe(res => {
        this.menus = this.menuService.getMenusByIdRestaurant(+this.restaurant.id);
      });
    }

    //Avis
    onSubmitAvis({ value, valid }: { value: Avis, valid: boolean }) {
      this.ajouterAvis(value);
    }

    openModalAvis(avis: Avis): void {
      this.selectedAvis = avis;
    }

    ajouterAvis(avis:Avis): void {
      avis.idRestaurant = this.restaurant.id;
      //  avis.idUtilisateur = this.utilisateur.id;
      this.avisService.create(avis)
      .subscribe(res => {
        this.allAvis = this.avisService.getAvisByIdRestaurant(+this.restaurant.id);
      });
    }


    supprimerAvis(): void {
      this.avisService
      .supprimer(this.selectedAvis.id)
      .subscribe(res => {
        this.allAvis = this.avisService.getAvisByIdRestaurant(+this.restaurant.id);
      });
    }


    goBack(): void {
      this.location.back();
    }

  }
