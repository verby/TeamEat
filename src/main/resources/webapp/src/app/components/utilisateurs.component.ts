import { Component,OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {Location} from '@angular/common';
import { Observable }     from 'rxjs/Observable';
import {Utilisateur} from '../models/utilisateur';
import {UtilisateurService} from '../services/utilisateur.service'

declare var $: any
@Component({
  selector:'utilisateurs',
  templateUrl: './utilisateurs.component.html',
  styleUrls: [ './utilisateurs.component.css' ]
})

export class UtilisateursComponent implements OnInit {

  utilisateurs: Observable<Utilisateur[]>;
  selectedUtilisateur: Utilisateur;
  utilisateur:FormGroup;
  res: Object;
  vehicule : boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private utilisateurService : UtilisateurService,
    private location: Location,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.utilisateur=this.fb.group({
      nom: ['', [Validators.required, Validators.minLength(2)]],
      prenom: ['', [Validators.required, Validators.minLength(2)]],
      mail: ['', [Validators.required, Validators.minLength(2)]],
      motdepasse: ['', [Validators.required, Validators.minLength(2)]],
      vehicule: ['']
    })
    this.route.params
    .subscribe(params => {
      this.utilisateurs = this.utilisateurService.getUtilisateurs();
    });
  }

  onSubmit({ value, valid }: { value: Utilisateur, valid: boolean }) {
    if(value.vehicule == ""){
      value.vehicule = "false";
    }
    this.ajouter(value);
  }

  openModal(utilisateur: Utilisateur): void {
    this.selectedUtilisateur = utilisateur;
  }

  goBack(): void {
    this.location.back();
  }

  ajouter(utilisateur:Utilisateur): void {
    this.utilisateurService.create(utilisateur)
    .subscribe(res  => this.utilisateurs = this.utilisateurService.getUtilisateurs());
  }

  supprimer(): void {
    this.utilisateurService
    .supprimer(this.selectedUtilisateur.id)
    .subscribe(res  => this.utilisateurs = this.utilisateurService.getUtilisateurs());
  }

  getVehicule(vehicule:boolean):string{
    let icone:string;
    this.vehicule=vehicule;
    if(vehicule){
      icone='verified_user'
    }else{
        icone='not_interested'
    }
    return icone;
  }


}
