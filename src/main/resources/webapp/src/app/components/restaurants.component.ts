import { Component,OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {Location} from '@angular/common';
import { Observable }     from 'rxjs/Observable';
import {Restaurant} from '../models/restaurant';
import {RestaurantService} from '../services/restaurant.service'

declare var $: any
@Component({
    selector:'restaurants',
    templateUrl: './restaurants.component.html',
    styleUrls: [ './restaurants.component.css' ]
  })

  export class RestaurantsComponent implements OnInit {

      restaurants: Observable<Restaurant[]>;
      selectedRestaurant: Restaurant;
      restaurant:FormGroup;
      res: Object;

      constructor(
        private route: ActivatedRoute,
        private router: Router,
        private restaurantService : RestaurantService,
        private location: Location,
        private fb: FormBuilder
      ) {}

      ngOnInit(): void {
        this.restaurant=this.fb.group({
           nom: ['', [Validators.required, Validators.minLength(2)]],
           adresse: ['', [Validators.required, Validators.minLength(2)]],
           telephone: ['', [Validators.required, Validators.minLength(2)]]
        })
        this.route.params
          .subscribe(params => {
          this.restaurants = this.restaurantService.getRestaurants();
        });
      }

        onSubmit({ value, valid }: { value: Restaurant, valid: boolean }) {
          this.ajouter(value);
        }

        openModal(restaurant: Restaurant): void {
          this.selectedRestaurant = restaurant;
       }

       ajouter(restaurant:Restaurant): void {
           restaurant.statut = 'VALIDE';
           restaurant.horaires = '2,3,4,5,6,7';
           this.restaurantService.create(restaurant)
            .subscribe(res  => this.restaurants = this.restaurantService.getRestaurants());
        }

        goBack(): void {
         this.location.back();
        }

        supprimer(): void {
          this.restaurantService
              .supprimer(this.selectedRestaurant.id)
              .subscribe(res  => this.restaurants = this.restaurantService.getRestaurants());
        }


      }
