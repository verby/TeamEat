import { Component,OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Observable }     from 'rxjs/Observable';
import {Commande} from '../models/commande';
import {CommandeService} from '../services/commande.service'
import {Menu} from '../models/menu';
import {MenuService} from '../services/menu.service'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
    selector:'commande-detail',
    templateUrl: './commande-detail.component.html',
    styleUrls: [ './commande-detail.component.css' ]
  })

  export class CommandeDetailComponent implements OnInit {

      commande: Commande;
      commandeGroup:FormGroup;
      selectedCommande: Commande;
      commandes: Observable<Commande[]>;
      menus: Observable<Menu[]>;
      res: Object;
      val: boolean;

      constructor(
        private route: ActivatedRoute,
        private commandeService : CommandeService,
        private menuService : MenuService,
        private location: Location,
        private fb: FormBuilder
      ) {}

      ngOnInit(): void {
        this.commandeGroup=this.fb.group({
          idMenu:['',[Validators.required, Validators.required]],
          conducteur:  [''],
          accompagne:  [''],
          paye:  [''],
          commentaire: [''],
          quantite:  ['1']
        })

        this.route.params
        .switchMap((params: Params) => this.commandeService.getCommande(+params['id']))
        .subscribe(commande => this.commande = commande);

        //Chargement des menus
          this.route.params
          .subscribe(params => {
            this.menus = this.menuService.getMenusByIdCommande(+params['id']);
          });

          //Chargement des commandes
            this.route.params
            .subscribe(params => {
              this.commandes = this.commandeService.getCommandesByIdParent(+params['id']);
            });

      }

      //Commandes
      onSubmit({ value, valid }: { value: Commande, valid: boolean }) {
        //Initialistion des variables idUtilisateurs et idMenu

        value.idUtilisateur=+localStorage.getItem("utilisateur");
        this.ajouter(value);
      }

      openModal(commande: Commande): void {
        this.selectedCommande = commande;
      }

      ajouter(commande: Commande): void {
        commande.dateCommande = this.commande.dateCommande;
        commande.nom = this.commande.nom;
        commande.idParent=this.commande.id;

        this.commandeService.create(commande)
        .subscribe(res => {
          this.commandes = this.commandeService.getCommandesByIdParent(commande.idParent);
        });
      }

      supprimer(): void {
        this.commandeService
            .supprimer(this.selectedCommande.id)
            .subscribe(res => {
              this.commandes = this.commandeService.getCommandesByIdParent(this.commande.id);
            });
      }

      //Le boutons
      goBack(): void {
        this.location.back();
      }

      save(): void {
        this.route.params
        .switchMap((params: Params) => this.commandeService.update(this.commande))
        .subscribe(() => this.goBack());
      }


      getIcone(val:boolean):string{
        let icone:string;
        this.val=val;
        if(val){
          icone='verified_user'
        }else{
            icone='not_interested'
        }
        return icone;
      }

    }
