import { Component,OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Observable }     from 'rxjs/Observable';
import {Utilisateur} from '../models/utilisateur';
import {UtilisateurService} from '../services/utilisateur.service'


@Component({
    selector:'utilisateur-detail',
    templateUrl: './utilisateur-detail.component.html',
    styleUrls: [ './utilisateur-detail.component.css' ]
  })

  export class UtilisateurDetailComponent implements OnInit {

      utilisateur: Utilisateur;
        vehicule : boolean;

      constructor(
        private route: ActivatedRoute,
        private utilisateurService : UtilisateurService,
        private location: Location
      ) {}

      ngOnInit(): void {
        this.route.params
        .switchMap((params: Params) => this.utilisateurService.getUtilisateur(+params['id']))
        .subscribe(utilisateur => this.utilisateur = utilisateur);
      }

      goBack(): void {
        this.location.back();
      }

      save(): void {
        this.route.params
        .switchMap((params: Params) => this.utilisateurService.update(this.utilisateur))
        .subscribe(() => this.goBack());
      }

      getVehicule(vehicule:boolean):string{
        let icone:string;
        this.vehicule=vehicule;
        if(vehicule){
          icone='verified_user'
        }else{
            icone='not_interested'
        }
        return icone;
      }

    }
