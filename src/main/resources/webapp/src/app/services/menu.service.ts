import { Injectable }    from '@angular/core';
import { Headers,RequestOptions, Http, URLSearchParams, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import { Menu } from '../models/menu';

@Injectable()
export class MenuService {

  private menusUrl = 'http://localhost:8080/teameat/api/menu';
    headers: Headers;
    options: RequestOptions;

  constructor(private http: Http) {
    this.headers = new Headers({ 'Content-Type': 'application/json'});
    this.options = new RequestOptions({ headers: this.headers });
  }

      //Récuperer un menu
    getMenu(id: number): Observable<Menu> {
       const url = `${this.menusUrl}/${id}`;
       return this.http.get(url)
       .map(this.extractData).catch(this.handleError);
     }

      //Récuperer tous les menus d'un restaurant
    getMenusByIdRestaurant(id:number): Observable<Menu[]> {
        const url = `${this.menusUrl}byrestaurant/${id}`;

        return this.http
                   .get(url)
                   .map(this.extractData).catch(this.handleError);
    }

    //Récuperer tous les menus d'un restaurant
  getMenusByIdCommande(id:number): Observable<Menu[]> {
      const url = `${this.menusUrl}bycommande/${id}`;

      return this.http
                 .get(url)
                 .map(this.extractData).catch(this.handleError);
  }

    private extractData(res: Response) {
      let body = res.json();
      return body || { };
    }

    private handleError (error: Response | any) {
      console.error(error);
      return Observable.throw(error);
    }

      //Créer un menu
      create(menu:Menu): Observable<Menu> {
      const url = `${this.menusUrl}`;
      return this.http
                 .post(url,menu ,{headers: this.headers})
                 .map(this.extractData).catch(this.handleError);
               }

    //Mettre à jour un menu
      update(menu:Menu): Observable<Menu> {
      const url = `${this.menusUrl}/${menu.id}`;
      return this.http
                 .put(url,menu, {headers: this.headers})
                 .map(this.extractData).catch(this.handleError);
    }

    //Supprimer un menu
      supprimer(id:number): Observable <void> {
        const url = `${this.menusUrl}/${id}`;
        return this.http
                   .delete(url,{headers: this.headers})
                   .map(this.extractData).catch(this.handleError);
      }
}
