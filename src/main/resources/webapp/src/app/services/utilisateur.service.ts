import { Injectable }    from '@angular/core';
import { Headers,RequestOptions, Http, URLSearchParams, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import { Utilisateur } from '../models/utilisateur';

@Injectable()
export class UtilisateurService {

  private utilisateursUrl = 'http://localhost:8080/teameat/api/utilisateur';
    headers: Headers;
    options: RequestOptions;

  constructor(private http: Http) {
    this.headers = new Headers({ 'Content-Type': 'application/json'});
    this.options = new RequestOptions({ headers: this.headers });
  }

      //Récuperer un utilisateur
    getUtilisateur(id: number): Observable<Utilisateur> {
       const url = `${this.utilisateursUrl}/${id}`;
       return this.http.get(url)
       .map(this.extractData).catch(this.handleError);
     }

      //Récuperer tous les utilisateurs
    getUtilisateurs(): Observable<Utilisateur[]> {
        return this.http
                   .get(this.utilisateursUrl)
                   .map(this.extractData).catch(this.handleError);
    }

    private extractData(res: Response) {
      let body = res.json();
      return body || { };
    }

    private handleError (error: Response | any) {
      console.error(error);
      return Observable.throw(error);
    }

      //Créer un utilisateur
      create(utilisateur:Utilisateur): Observable<Utilisateur> {
      const url = `${this.utilisateursUrl}`;
      return this.http
                 .post(url,utilisateur ,{headers: this.headers})
                 .map(this.extractData).catch(this.handleError);
               }

    //Mettre à jour un utilisateur
      update(utilisateur:Utilisateur) : Observable <Utilisateur> {
      const url = `${this.utilisateursUrl}/${utilisateur.id}`;
      return this.http
                 .put(url,utilisateur, {headers: this.headers})
                 .map(this.extractData).catch(this.handleError);
    }

    //Supprimer un utilisateur
      supprimer(id:number): Observable <void> {
        const url = `${this.utilisateursUrl}/${id}`;
        return this.http
                   .delete(url,{headers: this.headers})
                   .map(this.extractData).catch(this.handleError);
      }

      // private helper methods

      private jwt() {
      // create authorization header with jwt token
          let currentUser = JSON.parse(localStorage.getItem('currentUser'));
          if (currentUser && currentUser.token) {
              let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
              return new RequestOptions({ headers: headers });
            }
        }


}
