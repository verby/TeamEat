import { Injectable }    from '@angular/core';
import { Headers,RequestOptions, Http, URLSearchParams, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import { Commande } from '../models/commande';

@Injectable()
export class CommandeService {

  private commandesUrl = 'http://localhost:8080/teameat/api/commande';
    headers: Headers;
    options: RequestOptions;

  constructor(private http: Http) {
    this.headers = new Headers({ 'Content-Type': 'application/json'});
    this.options = new RequestOptions({ headers: this.headers });
  }

      //Récuperer une commande
    getCommande(id: number): Observable<Commande> {
       const url = `${this.commandesUrl}/${id}`;
       return this.http.get(url)
       .map(this.extractData).catch(this.handleError);
     }

      //Récuperer toutes les commandes
    getCommandes(): Observable<Commande[]> {
        return this.http
                   .get(this.commandesUrl)
                   .map(this.extractData).catch(this.handleError);
    }

    //Récuperer les sous commandes
      getCommandesByIdParent(id:number): Observable <Commande[]> {
           const url = `${this.commandesUrl}/parent/${id}`;
           return this.http
                    .get(url)
                    .map(this.extractData).catch(this.handleError);
      }

    private extractData(res: Response) {
      let body = res.json();
      return body || { };
    }

    private handleError (error: Response | any) {
      console.error(error);
      return Observable.throw(error);
    }

      //Créer une commande
      create(commande:Commande): Observable<Commande> {
      const url = `${this.commandesUrl}`;
      return this.http
                 .post(url,commande ,{headers: this.headers})
                 .map(this.extractData).catch(this.handleError);
               }

    //Mettre à jour une commande
      update(commande:Commande): Observable<Commande> {
      const url = `${this.commandesUrl}/${commande.id}`;
      return this.http
                 .put(url,commande, {headers: this.headers})
                 .map(this.extractData).catch(this.handleError);
    }

    //Supprimer une commande
      supprimer(id:number): Observable <void> {
        const url = `${this.commandesUrl}/${id}`;
        return this.http
                   .delete(url,{headers: this.headers})
                   .map(this.extractData).catch(this.handleError);
      }



}
