import { Injectable}    from '@angular/core';
import { Headers,RequestOptions, Http, URLSearchParams, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import { Restaurant } from '../models/restaurant';


@Injectable()
export class RestaurantService {

  private restaurantsUrl = 'http://localhost:8080/teameat/api/restaurant';
    headers: Headers;
    options: RequestOptions;

  constructor(private http: Http) {
    this.headers = new Headers({ 'Content-Type': 'application/json'});
    this.options = new RequestOptions({ headers: this.headers });
  }


      //Récuperer un restaurant
    getRestaurant(id: number): Observable<Restaurant> {
       const url = `${this.restaurantsUrl}/${id}`;
       return this.http.get(url)
       .map(this.extractData).catch(this.handleError);
     }

      //Récuperer tous les restaurants
    getRestaurants(): Observable<Restaurant[]> {
        return this.http
                   .get(this.restaurantsUrl)
                   .map(this.extractData).catch(this.handleError);
    }

    private extractData(res: Response) {
      let body = res.json();
      return body || { };
    }

    private handleError (error: Response | any) {
      console.error(error);
      return Observable.throw(error);
    }

      //Créer un restaurant
      create(restaurant:Restaurant): Observable<Restaurant> {
      const url = `${this.restaurantsUrl}`;
      return this.http
                 .post(url,restaurant ,{headers: this.headers})
                 .map(this.extractData).catch(this.handleError);
               }

    //Mettre à jour un restaurant
      update(restaurant:Restaurant): Observable<Restaurant> {
      const url = `${this.restaurantsUrl}/${restaurant.id}`;
      return this.http
                 .put(url,restaurant, {headers: this.headers})
                 .map(this.extractData).catch(this.handleError);
    }

    //Supprimer un restaurant
      supprimer(id:number): Observable <void> {
        const url = `${this.restaurantsUrl}/${id}`;
        return this.http
                   .delete(url,{headers: this.headers})
                   .map(this.extractData).catch(this.handleError);
      }


}
