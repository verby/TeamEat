import { Injectable }    from '@angular/core';
import { Headers,RequestOptions, Http, URLSearchParams, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import { Avis } from '../models/avis';

@Injectable()
export class AvisService {

  private avisUrl = 'http://localhost:8080/teameat/api/avis';
    headers: Headers;
    options: RequestOptions;

  constructor(private http: Http) {
    this.headers = new Headers({ 'Content-Type': 'application/json'});
    this.options = new RequestOptions({ headers: this.headers });
  }

      //Récuperer un avis
    getAvis(id: number): Observable<Avis> {
       const url = `${this.avisUrl}/${id}`;
       return this.http.get(url)
       .map(this.extractData).catch(this.handleError);
     }

      //Récuperer tous les avis
      getAvisByIdRestaurant(id:number): Observable<Avis[]> {
        const url = `${this.avisUrl}` + 'byrestaurant/' + `${id}`;

        return this.http
                   .get(url)
                   .map(this.extractData).catch(this.handleError);
                 }

    private extractData(res: Response) {
      let body = res.json();
      return body || { };
    }

    private handleError (error: Response | any) {
      console.error(error);
      return Observable.throw(error);
    }

      //Créer un avis
      create(avis:Avis): Observable<Avis> {
      const url = `${this.avisUrl}`;
      return this.http
                 .post(url,avis ,{headers: this.headers})
                 .map(this.extractData).catch(this.handleError);
               }

    //Mettre à jour un avis
      update(avis:Avis): Observable<Avis> {
      const url = `${this.avisUrl}/${avis.id}`;
      return this.http
                 .put(url,avis, {headers: this.headers})
                 .map(this.extractData).catch(this.handleError);
    }

    //Supprimer un avis
      supprimer(id:number): Observable <void> {
        const url = `${this.avisUrl}/${id}`;
        return this.http
                   .delete(url,{headers: this.headers})
                   .map(this.extractData).catch(this.handleError);
      }

      //get Moyenne
      getNoteByIdRestaurant(id:number) {
        const url = `${this.avisUrl}` + 'byrestaurant/note/' + `${id}`;
        return this.http
                   .get(url)
                   .map(this.extractData).catch(this.handleError);
                 }


}
