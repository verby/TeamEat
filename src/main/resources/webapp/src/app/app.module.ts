import './rxjs-extensions';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, JsonpModule }    from '@angular/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {UtilisateursComponent} from './components/utilisateurs.component';
import {UtilisateurService} from './services/utilisateur.service';
import {UtilisateurDetailComponent} from './components/utilisateur-detail.component';
import {RestaurantsComponent} from './components/restaurants.component';
import {CommandesComponent} from './components/commandes.component';
import {CommandeDetailComponent} from './components/commande-detail.component';
import {DashboardComponent} from './components/dashboard.component';
import {RestaurantService} from './services/restaurant.service';
import {RestaurantDetailComponent} from './components/restaurant-detail.component';
import {AvisService} from './services/avis.service';
import {CommandeService} from './services/commande.service';
import {MenuService} from './services/menu.service';
import {MaterializeModule} from "angular2-materialize";
import {AuthModule} from "./auth/auth.module";

import { AgmCoreModule } from 'angular2-google-maps/core';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    JsonpModule,
    ReactiveFormsModule,
    MaterializeModule,
    AuthModule,
    AgmCoreModule.forRoot({
  apiKey: 'AIzaSyAddHvz9H2ecO85JEoiVX8oIpKj9xtQcW4'
})
  ],
  declarations: [
    AppComponent,
    UtilisateursComponent,
    UtilisateurDetailComponent,
    RestaurantsComponent,
    RestaurantDetailComponent,
    CommandesComponent,
    CommandeDetailComponent,
    DashboardComponent
  ],
  providers: [
    UtilisateurService,
    RestaurantService,
    AvisService,
    MenuService,
    CommandeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
