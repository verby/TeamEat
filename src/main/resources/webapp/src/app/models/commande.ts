export class Commande{
  id:number;
  idUtilisateur:number;
  idMenu:number;
  idParent:number;
  conducteur:string;
  accompagne:string;
  nom:string;
  dateCommande:Date;
  quantite:number;
  statut:string;
  paye:string;
  commantaire:string;
  nomUtilisateur:string;
  nomRestaurant:string;
  nomMenu:string;
}
