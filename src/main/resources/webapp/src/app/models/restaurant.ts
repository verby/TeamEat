import {Menu} from '../models/menu';
import {Avis} from '../models/avis';

export class Restaurant{
  id:number;
  menus:Menu[];
  avis:Avis[];
  nom:string;
  adresse:string;
  telephone:string;
  horaires:string;
  statut:string;
}
