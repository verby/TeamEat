import {Commande} from '../models/commande';
import {Avis} from '../models/avis';

export class Utilisateur{
  id:number;
  avis:Avis[];
  commandes:Commande[];
  nom:string;
  prenom:string;
  mail:string;
  motdepasse:string;
  vehicule:string;


  public static isNull(utilisateur: Utilisateur): boolean {
          return utilisateur.mail === null &&
              utilisateur.motdepasse === null &&
              utilisateur.nom === null &&
              utilisateur.prenom === null;
      }

}
