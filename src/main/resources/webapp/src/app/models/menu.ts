export class Menu{
  id:number;
  idRestaurant:number;
  nom:string;
  description:string;
  image:string;
  prix:number;
}
