export class Avis{
  id:number;
  idRestaurant:number;
  idUtilisateur:number;
  dateAvis:Date;
  commentaire:string;
  nomUtilisateur:string;
  note:number;
}
