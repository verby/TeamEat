import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent }   from './app.component';
import { UtilisateursComponent }   from './components/utilisateurs.component';
import {UtilisateurDetailComponent} from './components/utilisateur-detail.component';
import { RestaurantsComponent }   from './components/restaurants.component';
import { CommandesComponent }   from './components/commandes.component';
import {RestaurantDetailComponent} from './components/restaurant-detail.component';
import {CommandeDetailComponent} from './components/commande-detail.component';
import {AuthComponent} from "./auth/auth.component";
import {DashboardComponent} from "./components/dashboard.component";
import {AuthGuard} from "./auth/services/auth-guard.service";

const routes: Routes = [
  {
    path: '',
    redirectTo: '/auth',
    pathMatch: 'full'
  },
  {
    path: 'auth',
    component: AuthComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'utilisateurs',
    component: UtilisateursComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'utilisateur/:id',
    component:UtilisateurDetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'restaurants',
    component: RestaurantsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'restaurant/:id',
    component:RestaurantDetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'commandes',
    component:CommandesComponent ,
    canActivate: [AuthGuard]
  },
  {
    path: 'commande/:id',
    component:CommandeDetailComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
