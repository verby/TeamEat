import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";

import { Observable }     from 'rxjs/Observable';
import {UtilisateurService} from './../../services/utilisateur.service'
import {AuthService} from "../services/auth.service";
import {Utilisateur} from "./../../models/utilisateur";
import { ActivatedRoute, Params }   from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

declare var $: any
@Component({
  moduleId: module.id,
  selector: 'login',
  templateUrl: './login-form.component.html',
  styleUrls: [ './login-form.component.css' ]
})
export class LoginFormComponent implements OnInit {
  utilisateurs: Observable<Utilisateur[]>;
  selectedUtilisateur: Utilisateur;
  utilisateur:FormGroup;
  res: Object;
  model: Utilisateur;
  error = '';

  constructor(private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private utilisateurService : UtilisateurService) {
    }

    ngOnInit(): void {
      this.model = new Utilisateur();
      this.utilisateur=this.fb.group({
        nom: ['', [Validators.required, Validators.minLength(2)]],
        prenom: ['', [Validators.required, Validators.minLength(2)]],
        mail: ['', [Validators.required, Validators.minLength(2)]],
        motdepasse: ['', [Validators.required, Validators.minLength(2)]],
        vehicule: ['']
      })
      this.route.params
      .subscribe(params => {
        this.utilisateurs = this.utilisateurService.getUtilisateurs();
      });
    }

    onSubmit(): void {
      this.authService
      .login(this.model)
      .subscribe(isLoggedIn => {
        if (isLoggedIn) {
          this.router.navigate(['/dashboard']);
        }
        else  {
          this.error = 'Login ou mot de passe incorrect';
        }
      });
    }

    onSubmitUtilisateur({ value, valid }: { value: Utilisateur, valid: boolean }) {
      if(value.vehicule == ""){
        value.vehicule = "false";
      }
      this.ajouter(value);
    }


    ajouter(utilisateur:Utilisateur): void {
      this.utilisateurService.create(utilisateur)
      .subscribe(res  => this.utilisateurs = this.utilisateurService.getUtilisateurs());
    }


    openModal(utilisateur: Utilisateur): void {
      this.selectedUtilisateur = utilisateur;
    }



  }
