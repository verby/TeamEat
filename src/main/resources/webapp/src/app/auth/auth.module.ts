import {NgModule} from "@angular/core";

import {CommonModule} from "@angular/common";

import {MaterializeModule} from "angular2-materialize";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {AuthComponent} from "./auth.component";
import {LoginFormComponent} from "./login-form/login-form.component";

import {AuthService} from "./services/auth.service";
import {AuthGuard} from "./services/auth-guard.service";

import {HttpModule,JsonpModule } from "@angular/http";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        JsonpModule,
        ReactiveFormsModule,
          MaterializeModule

    ],
    providers: [AuthService, AuthGuard],
    declarations: [AuthComponent, LoginFormComponent],
    exports: [AuthComponent]
})
export class AuthModule {
}
