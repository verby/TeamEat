import {Injectable} from "@angular/core";
import {Http} from "@angular/http";

import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';

import {Utilisateur} from "./../../models/utilisateur";
import {API_URL} from "./../../core/core.module";

@Injectable()
export class AuthService {

    isLoggedIn: boolean = false;
    error = '';

    constructor(private http: Http) {
    }

    login(utilisateur:Utilisateur): Observable<boolean> {
        return this.http.post(API_URL + "/login",utilisateur)
        .map(response => response.json() as Utilisateur)
        .map(utilisateur => {
            if (!Utilisateur.isNull(utilisateur)) {
                localStorage.setItem("utilisateur", utilisateur.id.toString());
                this.isLoggedIn = true;
                return true;
            } else {
                this.isLoggedIn = false;
                this.error = 'Login ou mot de passe sont incorrects';
                return false;
            }
        })
        .catch(AuthService.handleError);
    }

    register(utilisateur: Utilisateur): Observable<boolean> {
        return this.http.post(API_URL + "/register", utilisateur)
        .map(response => response.json() as Utilisateur)
        .map(utilisateur => !Utilisateur.isNull(utilisateur))
        .catch(AuthService.handleError);
    }

    private static handleError(error: any) {
        let errorMessage;
        if(error.message) {
            errorMessage = error.message
        } else {
            if(error.status) {
                errorMessage = `${error.status} - ${error.statusText}`
            } else {
                errorMessage = `Server error`
            }
        }

        if(error.status == '403') {
            alert('Login ou mot de passe sont incorrects');
            errorMessage= 'Login ou mot de passe sont incorrects';
        }

        return Observable.throw(errorMessage);
    }
}
