import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent{
  title="TeamEat"

  constructor(private router: Router) {
        }

  logout() {
      localStorage.removeItem("utilisateur");
      location.reload();
      this.router.navigate(['/auth']);
   }
}
