package fr.edt.teameat.service;
//Contrat

import java.util.List;

import fr.edt.teameat.model.Avis;


public interface AvisService {

	Avis findById(Long id);

	void saveAvis(Avis avis);

	void updateAvis(Avis avis);

	void deleteAvisById(Long id);

	void deleteAllAvis();

	List<Avis> findAllAvis();

	List<Avis> findByIdRestaurant(Long id);
}