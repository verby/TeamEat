package fr.edt.teameat.service;
//Contrat

import java.util.List;

import fr.edt.teameat.model.Restaurant;

public interface RestaurantService {

	Restaurant findById(Long id);

	Restaurant findByNom(String nom);

	void saveRestaurant(Restaurant restaurant);

	void updateRestaurant(Restaurant restaurant);

	void deleteRestaurantById(Long id);

	void deleteAllRestaurants();

	List<Restaurant> findAllRestaurants();

	boolean isRestaurantExist(Restaurant restaurant);
}