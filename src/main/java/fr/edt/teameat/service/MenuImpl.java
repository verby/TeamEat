package fr.edt.teameat.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.edt.teameat.model.Menu;
import fr.edt.teameat.repositories.MenuRepository;

@Service("menuService")
@Transactional
public class MenuImpl implements MenuService {

	@Autowired
	private MenuRepository menuRepository;

	@Override
	public Menu findById(Long id) {
		return menuRepository.findOne(id);
	}

	@Override
	public Menu findByNom(String nom) {
		return menuRepository.findByNom(nom);
	}

	@Override
	public List<Menu> findByIdRestaurant(Long id) {
		return menuRepository.findByIdRestaurant(id);
	}

	@Override
	public void saveMenu(Menu menu) {
		menuRepository.save(menu);
	}

	@Override
	public void updateMenu(Menu menu){
		saveMenu(menu);
	}

	@Override
	public void deleteMenuById(Long id) {
		menuRepository.delete(id);
	}

	@Override
	public void deleteAllMenus(){
		menuRepository.deleteAll();
	}

	@Override
	public List<Menu> findAllMenus(){
		return menuRepository.findAll();
	}

	@Override
	public boolean isMenuExist(Menu menu) {
		return findByNom(menu.getNom()) != null;
	}

}
