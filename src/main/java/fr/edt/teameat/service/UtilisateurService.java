package fr.edt.teameat.service;
//Contrat

import java.util.List;

import fr.edt.teameat.model.Utilisateur;

public interface UtilisateurService {

	Utilisateur findById(Long id);

	Utilisateur findByNom(String nom);

	Utilisateur saveUtilisateur(Utilisateur utilisateur);

	void updateUtilisateur(Utilisateur utilisateur);

	void deleteUtilisateurById(Long id);

	void deleteAllUtilisateurs();

	List<Utilisateur> findAllUtilisateurs();

	boolean isUtilisateurExist(Utilisateur utilisateur);

	Utilisateur findUtilisateurByMailAndMotdepasse(String mail, String motdepasse);

	Utilisateur findUtilisateurByMail(String mail);

}