package fr.edt.teameat.service;
//Contrat

import java.util.List;

import fr.edt.teameat.model.Commande;

public interface CommandeService {

	Commande findById(Long id);

	void saveCommande(Commande commande);

	void updateCommande(Commande commande);

	void deleteCommandeById(Long id);

	void deleteAllCommandes();

	List<Commande> findAllCommandes();

	List<Commande> findByIdParent(Long idParent);
}