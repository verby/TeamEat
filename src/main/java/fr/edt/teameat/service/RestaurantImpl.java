package fr.edt.teameat.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.edt.teameat.model.Restaurant;
import fr.edt.teameat.repositories.RestaurantRepository;

@Service("resraurantService")
@Transactional
public class RestaurantImpl implements RestaurantService {

	@Autowired
	private RestaurantRepository restaurantRepository;

	@Override
	public Restaurant findById(Long id) {
		return restaurantRepository.findOne(id);
	}
	@Override
	public Restaurant findByNom(String nom) {
		return restaurantRepository.findByNom(nom);
	}

	@Override
	public void saveRestaurant(Restaurant restaurant) {
		restaurantRepository.save(restaurant);
	}

	@Override
	public void updateRestaurant(Restaurant restaurant){
		saveRestaurant(restaurant);
	}

	@Override
	public void deleteRestaurantById(Long id) {
		restaurantRepository.delete(id);
	}

	@Override
	public void deleteAllRestaurants(){
		restaurantRepository.deleteAll();
	}

	@Override
	public List<Restaurant> findAllRestaurants(){
		return restaurantRepository.findAll();
	}
	@Override
	public boolean isRestaurantExist(Restaurant restaurant) {
		return findByNom(restaurant.getNom()) != null;
	}

}
