package fr.edt.teameat.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.edt.teameat.model.Avis;
import fr.edt.teameat.repositories.AvisRepository;

@Service("avisService")
@Transactional
public class AvisImpl implements AvisService {

	@Autowired
	private AvisRepository avisRepository;

	@Override
	public Avis findById(Long id) {
		return avisRepository.findOne(id);
	}

	@Override
	public void saveAvis(Avis avis) {
		avisRepository.save(avis);
	}

	@Override
	public void updateAvis(Avis avis){
		saveAvis(avis);
	}

	@Override
	public void deleteAvisById(Long id) {
		avisRepository.delete(id);
	}

	@Override
	public void deleteAllAvis(){
		avisRepository.deleteAll();
	}

	@Override
	public List<Avis> findAllAvis(){
		return avisRepository.findAll();
	}

	@Override
	public List<Avis> findByIdRestaurant(Long id) {
		return avisRepository.findByIdRestaurant(id);
	}

}
