package fr.edt.teameat.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.edt.teameat.model.Utilisateur;
import fr.edt.teameat.repositories.UtilisateurRepository;


@Service("utilisateurService")
@Transactional
public class UtilisateurImpl implements UtilisateurService {

	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@Override
	public Utilisateur findById(Long id) {
		return utilisateurRepository.findOne(id);
	}

	@Override
	public Utilisateur findByNom(String nom) {
		return utilisateurRepository.findByNom(nom);
	}

	@Override
	public Utilisateur saveUtilisateur(Utilisateur utilisateur) {
		return utilisateurRepository.save(utilisateur);
	}

	@Override
	public void updateUtilisateur(Utilisateur utilisateur){
		saveUtilisateur(utilisateur);
	}

	@Override
	public void deleteUtilisateurById(Long id){
		utilisateurRepository.delete(id);
	}

	@Override
	public void deleteAllUtilisateurs(){
		utilisateurRepository.deleteAll();
	}

	@Override
	public List<Utilisateur> findAllUtilisateurs(){
		return utilisateurRepository.findAll();
	}

	@Override
	public boolean isUtilisateurExist(Utilisateur utilisateur) {
		return findByNom(utilisateur.getNom()) != null;
	}

	@Override
	public Utilisateur findUtilisateurByMailAndMotdepasse(String mail, String motdepasse) {
		return utilisateurRepository.findOneByMailAndMotdepasse(mail, motdepasse);
	}

	@Override
	public Utilisateur findUtilisateurByMail(String mail) {
		return utilisateurRepository.findOneByMail(mail);
	}

}
