package fr.edt.teameat.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.edt.teameat.model.Commande;
import fr.edt.teameat.repositories.CommandeRepository;

@Service("CommandeService")
@Transactional

public class CommandeImpl implements CommandeService{

	@Autowired
	private CommandeRepository commandeRepository;

	@Override
	public Commande findById(Long id) {
		return commandeRepository.findOne(id);
	}

	@Override
	public void saveCommande(Commande commande) {
		commandeRepository.save(commande);
	}

	@Override
	public void updateCommande(Commande commande){
		saveCommande(commande);
	}

	@Override
	public void deleteCommandeById(Long id) {
		commandeRepository.delete(id);
	}

	@Override
	public void deleteAllCommandes(){
		commandeRepository.deleteAll();
	}

	@Override
	public List<Commande> findAllCommandes(){
		return commandeRepository.findAll();
	}

	@Override
	public List<Commande> findByIdParent(Long idParent) {
		return commandeRepository.findByIdParent(idParent);
	}

}
