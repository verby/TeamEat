package fr.edt.teameat.service;
//Contrat

import java.util.List;

import fr.edt.teameat.model.Menu;

public interface MenuService {

	Menu findById(Long id);

	List<Menu> findByIdRestaurant(Long id);

	Menu findByNom(String nom);

	void saveMenu(Menu menu);

	void updateMenu(Menu menu);

	void deleteMenuById(Long id);

	void deleteAllMenus();

	List<Menu> findAllMenus();

	boolean isMenuExist(Menu menu);
}