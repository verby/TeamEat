package fr.edt.teameat.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Avis")


public class Avis implements Serializable {

	// An autogenerated id (unique for each user in the db)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;

	@Column(name="idRestaurant")
	private Long idRestaurant;

	@Column(name="idUtilisateur")
	private Long idUtilisateur;

	@Column(name="dateAvis")
	private Date dateAvis;

	@Column(name = "commentaire")
	private String commentaire;

	private String nomUtilisateur;

	@Column(name = "note")
	private Integer note;

	//Constructeur par default
	public Avis() {
		super();
		this.id = null;
		this.idRestaurant = null;
		this.idUtilisateur = null;
		this.dateAvis = null;
		this.commentaire = "";
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public Avis(Long id, Long idRestaurant, Long idUtilisateur, Date dateAvis, String commentaire) {
		super();
		this.id = id;
		this.idRestaurant = idRestaurant;
		this.idUtilisateur = idUtilisateur;
		this.dateAvis = dateAvis;
		this.commentaire = commentaire;
	}



	//
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdRestaurant() {
		return idRestaurant;
	}

	public void setIdRestaurant(Long idRestaurant) {
		this.idRestaurant = idRestaurant;
	}

	public Long getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(Long idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	public Date getDateAvis() {
		return dateAvis;
	}

	public void setDateAvis(Date dateAvis) {
		this.dateAvis = dateAvis;
	}

	public String getNomUtilisateur() {
		return nomUtilisateur;
	}

	public void setNomUtilisateur(String nomUtilisateur) {
		this.nomUtilisateur = nomUtilisateur;
	}

	public Integer getNote() {
		return note;
	}

	public void setNote(Integer note) {
		this.note = note;
	}

}
