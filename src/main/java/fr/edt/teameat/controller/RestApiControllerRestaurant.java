package fr.edt.teameat.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import fr.edt.teameat.model.Restaurant;
import fr.edt.teameat.service.AvisService;
import fr.edt.teameat.service.RestaurantService;
import fr.edt.teameat.util.CustomErrorType;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class RestApiControllerRestaurant {

	public static final Logger logger = LoggerFactory.getLogger(RestApiControllerRestaurant.class);

	@Autowired
	RestaurantService restaurantService;//Service which will do all data retrieval/manipulation work

	@Autowired
	AvisService avisService;

	// --------------Récupérer Tous les Restaurants-----------------------------
	@RequestMapping(value = "/restaurant", method = RequestMethod.GET)
	public ResponseEntity<List<Restaurant>> listAllRestaurants() {
		List<Restaurant> restaurants = restaurantService.findAllRestaurants();

		return new ResponseEntity<List<Restaurant>>(restaurants, HttpStatus.OK);
	}

	// ---------------Récupérer Restaurant--------------------------------

	@RequestMapping(value = "/restaurant/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getRestaurant(@PathVariable("id") Long id) {
		logger.info("Fetching Restaurant with id {}", id);
		Restaurant restaurant = restaurantService.findById(id);
		if (restaurant == null) {
			logger.error("Restaurant with id {} is not found.", id);
			return new ResponseEntity(new CustomErrorType("Restaurant with id " + id
					+ " is not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Restaurant>(restaurant, HttpStatus.OK);
	}

	// ----------------Créer Restaurant--------------------------------

	@RequestMapping(value = "/restaurant", method = RequestMethod.POST)
	public ResponseEntity<?> createRestaurant(@RequestBody Restaurant restaurant, UriComponentsBuilder ucBuilder) {
		logger.info("Creating Restaurant : {}", restaurant);

		if (restaurantService.isRestaurantExist(restaurant)) {
			logger.error("Unable to create. A Restaurant with name {} already exists", restaurant.getNom());
			return new ResponseEntity(new CustomErrorType("Unable to create. A Restaurant with name " +
					restaurant.getNom() + " already exists."),HttpStatus.CONFLICT);
		}
		restaurantService.saveRestaurant(restaurant);

		// HttpHeaders headers = new HttpHeaders();
		// headers.setLocation(ucBuilder.path("/api/restaurant/{id}").buildAndExpand(restaurant.getId()).toUri());
		return new ResponseEntity<Restaurant>(restaurant, HttpStatus.CREATED);
	}

	// --------------- Mettre à jour Restaurant------------------------

	@RequestMapping(value = "/restaurant/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateRestaurant(@PathVariable("id") Long id, @RequestBody Restaurant restaurant) {
		logger.info("Updating Restaurant with id {}", id);

		Restaurant currentRestaurant = restaurantService.findById(id);

		if (currentRestaurant == null) {
			logger.error("Unable to update. Restaurant with id {} is not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to update. Restaurant with id " + id + " is not found."),
					HttpStatus.NOT_FOUND);
		}
		// currentRestaurant.setId(restaurant.getId());
		currentRestaurant.setNom(restaurant.getNom());
		currentRestaurant.setAdresse(restaurant.getAdresse());
		currentRestaurant.setTelephone(restaurant.getTelephone());
		currentRestaurant.setHoraires(restaurant.getHoraires());
		currentRestaurant.setStatut(restaurant.getStatut());

		restaurantService.updateRestaurant(currentRestaurant);
		return new ResponseEntity<Restaurant>(currentRestaurant, HttpStatus.OK);
	}

	// ------------------- Supprimer Restaurant-----------------------------------------

	@RequestMapping(value = "/restaurant/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteRestaurant(@PathVariable("id") Long id) {
		logger.info("Fetching & Deleting Restaurant with id {}", id);

		Restaurant restaurant = restaurantService.findById(id);
		if (restaurant == null) {
			logger.error("Unable to delete. Restaurant with id {} is not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to delete. Restaurant with id " + id + " is not found."),
					HttpStatus.NOT_FOUND);
		}
		restaurantService.deleteRestaurantById(id);

		return new ResponseEntity<Restaurant>(HttpStatus.NO_CONTENT);
	}

	// ------------------- Supprimer Tous Restaurants-----------------------------

	@RequestMapping(value = "/restaurant", method = RequestMethod.DELETE)
	public ResponseEntity<Restaurant> deleteAllRestaurants() {
		logger.info("Deleting All Restaurants");

		restaurantService.deleteAllRestaurants();
		return new ResponseEntity<Restaurant>(HttpStatus.NO_CONTENT);
	}

}