package fr.edt.teameat.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import fr.edt.teameat.model.Commande;
import fr.edt.teameat.model.Menu;
import fr.edt.teameat.service.CommandeService;
import fr.edt.teameat.service.MenuService;
import fr.edt.teameat.util.CustomErrorType;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class RestApiControllerMenu {

	public static final Logger logger = LoggerFactory.getLogger(RestApiControllerMenu.class);

	@Autowired
	MenuService menuService;//Service which will do all data retrieval/manipulation work
	@Autowired
	CommandeService commandeService;

	// -----------------Récupérer Tous les Menus-----------------------------

	@RequestMapping(value = "/menu", method = RequestMethod.GET)
	public ResponseEntity<List<Menu>> listAllMenus() {
		List<Menu> menus = menuService.findAllMenus();

		return new ResponseEntity<List<Menu>>(menus, HttpStatus.OK);
	}

	// ----------------Récupérer Menu----------------------------------

	@RequestMapping(value = "/menu/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getMenus(@PathVariable("id") Long id) {
		logger.info("Fetching Menu with id {}", id);
		Menu menu = menuService.findById(id);
		if (menu == null) {
			logger.error("Menu with id {} is not found.", id);
			return new ResponseEntity(new CustomErrorType("Menu with id " + id
					+ " is not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Menu>(menu, HttpStatus.OK);
	}

	// ----------------Récupérer Menu d'après id du restaurant------------------
	@RequestMapping(value = "/menubyrestaurant/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getMenusByIdRestaurant(@PathVariable("id") Long id) {
		logger.info("Fetching Menu with id {}", id);
		List<Menu> menus = menuService.findByIdRestaurant(id);

		return new ResponseEntity<List<Menu>>(menus, HttpStatus.OK);
	}

	// ----------------Récupérer Menu d'après id d'une commande-----------------
	@RequestMapping(value = "/menubycommande/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getMenusByIdCommande(@PathVariable("id") Long id) {
		logger.info("Fetching Menu with id {}", id);
		Commande maCommande = commandeService.findById(id);
		return getMenusByIdRestaurant(Long.valueOf(maCommande.getNom()));
	}

	// -------------------Créer Menu-------------------------------------------

	@RequestMapping(value = "/menu", method = RequestMethod.POST)
	public ResponseEntity<?> createMenu(@RequestBody Menu menu, UriComponentsBuilder ucBuilder) {
		logger.info("Creating Menu : {}", menu);

		if (menuService.isMenuExist(menu)) {
			logger.error("Unable to create. A Menu with name {} already exists", menu.getNom());
			return new ResponseEntity(new CustomErrorType("Unable to create. A Menu with name " +
					menu.getNom() + " already exists."),HttpStatus.CONFLICT);
		}
		menuService.saveMenu(menu);
		// HttpHeaders headers = new HttpHeaders();
		// headers.setLocation(ucBuilder.path("/api/menu/{id}").buildAndExpand(menu.getId()).toUri());
		return new ResponseEntity<Menu>(menu, HttpStatus.CREATED);
	}

	// ---------------- Mettre à jour Menu------------------------------------

	@RequestMapping(value = "/menu/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateMenu(@PathVariable("id") Long id, @RequestBody Menu menu) {
		logger.info("Updating Menu with id {}", id);

		Menu currentMenu = menuService.findById(id);

		if (currentMenu == null) {
			logger.error("Unable to update. Menu with id {} is not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to update. Menu with id " + id + " is not found."),
					HttpStatus.NOT_FOUND);
		}
		currentMenu.setId(menu.getId());
		currentMenu.setNom(menu.getNom());
		currentMenu.setDescription(menu.getDescription());
		currentMenu.setImage(menu.getImage());
		currentMenu.setPrix(menu.getPrix());

		menuService.updateMenu(currentMenu);
		return new ResponseEntity<Menu>(currentMenu, HttpStatus.OK);
	}

	// ------------------- Supprimer Menu-----------------------------------------

	@RequestMapping(value = "/menu/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteMenu(@PathVariable("id") Long id) {
		logger.info("Fetching & Deleting Menu with id {}", id);

		Menu menu = menuService.findById(id);
		if (menu == null) {
			logger.error("Unable to delete. Menu with id {} is not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to delete. Menu with id " + id + " is not found."),
					HttpStatus.NOT_FOUND);
		}
		menuService.deleteMenuById(id);
		return new ResponseEntity<Menu>(HttpStatus.NO_CONTENT);
	}

	// ------------------- Supprimer Tous Menus-----------------------------

	@RequestMapping(value = "/menu", method = RequestMethod.DELETE)
	public ResponseEntity<Menu> deleteAllMenus() {
		logger.info("Deleting All Menus");

		menuService.deleteAllMenus();
		return new ResponseEntity<Menu>(HttpStatus.NO_CONTENT);
	}

}