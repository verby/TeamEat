package fr.edt.teameat.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import fr.edt.teameat.model.Avis;
import fr.edt.teameat.model.Utilisateur;
import fr.edt.teameat.service.AvisService;
import fr.edt.teameat.service.UtilisateurService;
import fr.edt.teameat.util.CustomErrorType;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class RestApiControllerAvis {

	public static final Logger logger = LoggerFactory.getLogger(RestApiControllerAvis.class);

	@Autowired
	AvisService avisService;//Service which will do all data retrieval/manipulation work
	@Autowired
	UtilisateurService utilisateurservice;

	// ------------------------Récupérer Tous les Avis----------

	@RequestMapping(value = "/avis", method = RequestMethod.GET)
	public ResponseEntity<List<Avis>> listAllAvis() {
		List<Avis> avis= avisService.findAllAvis();

		List<Avis> mesAvis = new ArrayList<Avis>();

		for (Avis unAvis : avis) {
			Utilisateur unUtilisateur = utilisateurservice.findById(unAvis.getIdUtilisateur());
			String prenom = unUtilisateur.getPrenom();
			String nom = unUtilisateur.getNom();
			unAvis.setNomUtilisateur(prenom + " " + nom);
			mesAvis.add(unAvis);
		}

		return new ResponseEntity<List<Avis>>(mesAvis, HttpStatus.OK);
	}

	// -------------Récupérer Avis------------------------------------------

	@RequestMapping(value = "/avis/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getAvis(@PathVariable("id") Long id) {
		logger.info("Fetching Avis with id {}", id);
		Avis avis = avisService.findById(id);
		if (avis == null) {
			logger.error("Avis with id {} is not found.", id);
			return new ResponseEntity(new CustomErrorType("Avis with id " + id
					+ " is not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Avis>(avis, HttpStatus.OK);
	}

	// ----------------Récupérer Avis d'après id du restaurant--------------

	@RequestMapping(value = "/avisbyrestaurant/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getAvisByIdRestaurant(@PathVariable("id") Long id) {
		logger.info("Fetching Avis with id {}", id);
		List<Avis> avis = avisService.findByIdRestaurant(id);

		List<Avis> mesAvis = new ArrayList<Avis>();

		for (Avis unAvis : avis) {
			Utilisateur unUtilisateur = utilisateurservice.findById(unAvis.getIdUtilisateur());
			String prenom = unUtilisateur.getPrenom();
			String nom = unUtilisateur.getNom();
			unAvis.setNomUtilisateur(prenom + " " + nom);
			mesAvis.add(unAvis);
		}

		return new ResponseEntity<List<Avis>>(mesAvis, HttpStatus.OK);
	}

	// ----------------Calculer Note Moyenne d'après id du restaurant-----

	@RequestMapping(value = "/avisbyrestaurant/note/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getNoteAvisByIdRestaurant(@PathVariable("id") Long id) {
		logger.info("Fetching Avis with id {}", id);
		List<Avis> avis = avisService.findByIdRestaurant(id);

		List<Avis> mesAvis = new ArrayList<Avis>();

		for (Avis unAvis : avis) {
			Utilisateur unUtilisateur = utilisateurservice.findById(unAvis.getIdUtilisateur());
			String prenom = unUtilisateur.getPrenom();
			String nom = unUtilisateur.getNom();
			unAvis.setNomUtilisateur(prenom + " " + nom);
			mesAvis.add(unAvis);
		}
		Integer moyenne = 0;
		for (Avis unAvis : mesAvis) {
			System.out.println(moyenne);
			System.out.println("note" + unAvis.getNote());
			moyenne = (moyenne + unAvis.getNote());

		}
		moyenne = moyenne / mesAvis.size();

		return new ResponseEntity<Integer>(moyenne, HttpStatus.OK);
	}

	// -------------------Créer Avis-------------------------------------------

	@RequestMapping(value = "/avis", method = RequestMethod.POST)
	public ResponseEntity<?> createAvis(@RequestBody Avis avis, UriComponentsBuilder ucBuilder) {
		logger.info("Creating Avis : {}", avis);

		/*	if (avisService.isAvisExist(avis)) {
			logger.error("Unable to create. A Avis with name {} already exists", avis.getNom());
			return new ResponseEntity(new CustomErrorType("Unable to create. A Avis with name " +
					avis.getNom() + " already exists."),HttpStatus.CONFLICT);
		}*/
		avisService.saveAvis(avis);

		// HttpHeaders headers = new HttpHeaders();
		// headers.setLocation(ucBuilder.path("/api/avis/{id}").buildAndExpand(avis.getId()).toUri());
		return new ResponseEntity<Avis>(avis, HttpStatus.CREATED);
	}

	// ----------- Mettre à jour Avis------------------------

	@RequestMapping(value = "/avis/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateAvis(@PathVariable("id") Long id, @RequestBody Avis avis) {
		logger.info("Updating Avis with id {}", id);

		Avis currentAvis = avisService.findById(id);

		if (currentAvis == null) {
			logger.error("Unable to update. Avis with id {} is not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to update. Avis with id " + id + " is not found."),
					HttpStatus.NOT_FOUND);
		}
		currentAvis.setId(avis.getId());
		currentAvis.setIdRestaurant(avis.getIdRestaurant());
		currentAvis.setIdUtilisateur(avis.getIdUtilisateur());
		currentAvis.setDateAvis(avis.getDateAvis());
		currentAvis.setCommentaire(avis.getCommentaire());

		avisService.updateAvis(currentAvis);
		return new ResponseEntity<Avis>(currentAvis, HttpStatus.OK);
	}

	// ------------------- Supprimer Avis-----------------------------------------

	@RequestMapping(value = "/avis/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteAvis(@PathVariable("id") Long id) {
		logger.info("Fetching & Deleting Avis with id {}", id);

		Avis avis = avisService.findById(id);
		if (avis == null) {
			logger.error("Unable to delete. Avis with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to delete. Avis with id " + id + " is not found."),
					HttpStatus.NOT_FOUND);
		}
		avisService.deleteAvisById(id);
		return new ResponseEntity<Avis>(HttpStatus.NO_CONTENT);
	}

	// ------------------- Supprimer Tous Avis-----------------------------

	@RequestMapping(value = "/avis", method = RequestMethod.DELETE)
	public ResponseEntity<Avis> deleteAllAvis() {
		logger.info("Deleting All Avis");

		avisService.deleteAllAvis();
		return new ResponseEntity<Avis>(HttpStatus.NO_CONTENT);
	}

}