package fr.edt.teameat.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import fr.edt.teameat.model.Commande;
import fr.edt.teameat.model.Menu;
import fr.edt.teameat.model.Restaurant;
import fr.edt.teameat.model.Utilisateur;
import fr.edt.teameat.service.CommandeService;
import fr.edt.teameat.service.MenuService;
import fr.edt.teameat.service.RestaurantService;
import fr.edt.teameat.service.UtilisateurService;
import fr.edt.teameat.util.CustomErrorType;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class RestApiControllerCommande {

	public static final Logger logger = LoggerFactory.getLogger(RestApiControllerCommande.class);

	@Autowired
	CommandeService commandeService;//Service which will do all data retrieval/manipulation work
	@Autowired
	UtilisateurService utilisateurservice;
	@Autowired
	RestaurantService restaurantService;
	@Autowired
	MenuService menuService;

	// ---------------Récupérer Toutes les Commandes---------------

	@RequestMapping(value = "/commande", method = RequestMethod.GET)
	public ResponseEntity<List<Commande>> listAllCommandes() {
		List<Commande> commandes = new ArrayList<Commande>();
		List<Commande> coms = commandeService.findAllCommandes();

		for (Commande com : coms) {
			if (com.getIdParent() == null) {
				Restaurant unRestaurant = restaurantService.findById(Long.parseLong(com.getNom()));
				String nom = unRestaurant.getNom();
				com.setNomRestaurant(nom);
				commandes.add(com);
			}
		}
		return new ResponseEntity<List<Commande>>(commandes, HttpStatus.OK);
	}

	// ------------Récupérer les Commandes d'après idParent------------
	@RequestMapping(value = "/commande/parent/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<Commande>> getCommandesByIdParent(@PathVariable("id") Long id) {
		List<Commande> commandes = commandeService.findByIdParent(id);

		List<Commande> mesCommandes = new ArrayList<Commande>();

		for (Commande commande : commandes) {
			Utilisateur unUtilisateur = utilisateurservice.findById(commande.getIdUtilisateur());
			String prenom = unUtilisateur.getPrenom();
			String nom = unUtilisateur.getNom();
			commande.setNomUtilisateur(prenom + " " + nom);

			// nomMenu
			Menu unMenu = menuService.findById(commande.getIdMenu());
			String nomMenu = unMenu.getNom();
			commande.setNomMenu(nomMenu);
			mesCommandes.add(commande);
		}
		return new ResponseEntity<List<Commande>>(mesCommandes, HttpStatus.OK);
	}


	// -------------Récupérer Commande-------------------------------

	@RequestMapping(value = "/commande/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getCommandes(@PathVariable("id") Long id) {
		logger.info("Fetching Commande with id {}", id);
		Commande commande = commandeService.findById(id);
		if (commande == null) {
			logger.error("Commande with id {} is not found.", id);
			return new ResponseEntity(new CustomErrorType("Commande with id " + id
					+ " is not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Commande>(commande, HttpStatus.OK);
	}

	// --------------Créer Commande---------------------------------

	@RequestMapping(value = "/commande", method = RequestMethod.POST)
	public ResponseEntity<?> createCommande(@RequestBody Commande commande, UriComponentsBuilder ucBuilder) {
		logger.info("Creating Commande : {}", commande);

		commandeService.saveCommande(commande);

		return new ResponseEntity<Commande>(commande, HttpStatus.CREATED);
	}

	// ------------ Mettre à jour Commande---------------------------------

	@RequestMapping(value = "/commande/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateCommande(@PathVariable("id") Long id, @RequestBody Commande commande) {
		logger.info("Updating Commande with id {}", id);

		Commande currentCommande = commandeService.findById(id);

		if (currentCommande == null) {
			logger.error("Unable to update. Commande with id {} is not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to update. Commande with id " + id + " is not found."),
					HttpStatus.NOT_FOUND);
		}
		// currentCommande.setId(commande.getId());
		// currentCommande.setIdUtilisateur(commande.getIdUtilisateur());
		// currentCommande.setIdMenu(commande.getIdMenu());
		// currentCommande.setIdParent(commande.getIdParent());
		currentCommande.setConducteur(commande.getConducteur());
		currentCommande.setAccompagne(commande.getAccompagne());
		currentCommande.setNom(commande.getNom());
		currentCommande.setDateCommande(commande.getDateCommande());
		currentCommande.setQuantite(commande.getQuantite());
		currentCommande.setStatut(commande.getStatut());
		currentCommande.setPaye(commande.getPaye());
		currentCommande.setCommentaire(commande.getCommentaire());


		commandeService.updateCommande(currentCommande);
		return new ResponseEntity<Commande>(currentCommande, HttpStatus.OK);
	}

	// ------------------- Supprimer Commande-----------------------------------------

	@RequestMapping(value = "/commande/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteCommande(@PathVariable("id") Long id) {
		logger.info("Fetching & Deleting Commande with id {}", id);

		Commande commande = commandeService.findById(id);
		if (commande == null) {
			logger.error("Unable to delete. Commande with id {} is not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to delete. Commande with id " + id + " is not found."),
					HttpStatus.NOT_FOUND);
		}
		commandeService.deleteCommandeById(id);
		return new ResponseEntity<Commande>(HttpStatus.NO_CONTENT);
	}

	// ------------------- Supprimer Tous Commandes-----------------------------

	@RequestMapping(value = "/commande", method = RequestMethod.DELETE)
	public ResponseEntity<Commande> deleteAllCommandes() {
		logger.info("Deleting All Commandes");

		commandeService.deleteAllCommandes();
		return new ResponseEntity<Commande>(HttpStatus.NO_CONTENT);
	}

}