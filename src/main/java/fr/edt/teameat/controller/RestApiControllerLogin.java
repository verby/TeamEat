package fr.edt.teameat.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import fr.edt.teameat.model.Utilisateur;
import fr.edt.teameat.service.UtilisateurService;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class RestApiControllerLogin {

	public static final Logger logger = LoggerFactory.getLogger(RestApiControllerLogin.class);

	private final UtilisateurService utilisateurService;

	@Autowired
	public RestApiControllerLogin(UtilisateurService utilisateurService) {
		this.utilisateurService = utilisateurService;
	}

	@RequestMapping(
			value = "/login",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> login(@RequestBody Utilisateur utilisateur, UriComponentsBuilder ucBuilder) {
		if (StringUtils.isEmpty(utilisateur.getMail()) || StringUtils.isEmpty(utilisateur.getMotdepasse())) {
			return new ResponseEntity<>(new Utilisateur(), HttpStatus.FORBIDDEN);
		}
		if (utilisateurService.findUtilisateurByMailAndMotdepasse(utilisateur.getMail(),
				utilisateur.getMotdepasse()) == null) {
			return new ResponseEntity<>(new Utilisateur(), HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity<>(utilisateurService.findUtilisateurByMail(utilisateur.getMail()),
				HttpStatus.OK);
	}
}
