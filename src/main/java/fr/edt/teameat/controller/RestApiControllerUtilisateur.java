package fr.edt.teameat.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import fr.edt.teameat.model.Utilisateur;
import fr.edt.teameat.service.UtilisateurService;
import fr.edt.teameat.util.CustomErrorType;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class RestApiControllerUtilisateur {

	public static final Logger logger = LoggerFactory.getLogger(RestApiControllerUtilisateur.class);

	@Autowired
	UtilisateurService utilisateurService;//Service which will do all data retrieval/manipulation work

	// -------------Récupérer Tous les Utilisateurs----------------------------

	@RequestMapping(value = "/utilisateur", method = RequestMethod.GET)
	public ResponseEntity<List<Utilisateur>> listAllUtilisateurs() {
		List<Utilisateur> utilisateurs = utilisateurService.findAllUtilisateurs();

		return new ResponseEntity<List<Utilisateur>>(utilisateurs, HttpStatus.OK);
	}

	// -------------Récupérer Utilisateur---------------------------
	@RequestMapping(value = "/utilisateur/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getUtilisateur(@PathVariable("id") Long id) {
		logger.info("Fetching Utilisateur with id {}", id);
		Utilisateur utilisateur = utilisateurService.findById(id);
		if (utilisateur == null) {
			logger.error("Utilisateur with id {} is not found.", id);
			return new ResponseEntity(new CustomErrorType("Utilisateur with id " + id
					+ " is not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Utilisateur>(utilisateur, HttpStatus.OK);
	}

	// --------------Créer Utilisateur---------------------------
	@RequestMapping(value = "/utilisateur", method = RequestMethod.POST)
	public ResponseEntity<?> createUtilisateur(@RequestBody Utilisateur utilisateur, UriComponentsBuilder ucBuilder) {
		logger.info("Creating Utilisateur : {}", utilisateur);

		if (utilisateurService.isUtilisateurExist(utilisateur)) {
			logger.error("Unable to create. A Utilisateur with name {} already exists", utilisateur.getNom());
			return new ResponseEntity(new CustomErrorType("Unable to create. A Utilisateur with name " +
					utilisateur.getNom() + " already exists."),HttpStatus.CONFLICT);
		}
		utilisateurService.saveUtilisateur(utilisateur);

		//HttpHeaders headers = new HttpHeaders();
		//headers.setLocation(ucBuilder.path("/api/utilisateur/{id}").buildAndExpand(utilisateur.getId()).toUri());
		return new ResponseEntity<Utilisateur>(utilisateur, HttpStatus.CREATED);
	}

	// ----- Mettre à jour Utilisateur-------------------------------------
	@RequestMapping(value = "/utilisateur/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateUtilisateur(@PathVariable("id") Long id, @RequestBody Utilisateur utilisateur) {
		logger.info("Updating Utilisateur with id {}", id);

		Utilisateur currentUtilisateur = utilisateurService.findById(id);

		if (currentUtilisateur == null) {
			logger.error("Unable to update. Utilisateur with id {} is not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to update. Utilisateur with id " + id + " is not found."),
					HttpStatus.NOT_FOUND);
		}
		// currentUtilisateur.setId(utilisateur.getId());
		currentUtilisateur.setNom(utilisateur.getNom());
		currentUtilisateur.setPrenom(utilisateur.getPrenom());
		currentUtilisateur.setMail(utilisateur.getMail());
		currentUtilisateur.setVehicule(utilisateur.getVehicule());

		utilisateurService.updateUtilisateur(currentUtilisateur);
		return new ResponseEntity<Utilisateur>(currentUtilisateur, HttpStatus.OK);
	}

	// ------------------- Supprimer Utilisateur-----------------------------------------
	@RequestMapping(value = "/utilisateur/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteUtilisateur(@PathVariable("id") Long id) {
		logger.info("Fetching & Deleting Utilisateur with id {}", id);

		Utilisateur utilisateur = utilisateurService.findById(id);
		if (utilisateur == null) {
			logger.error("Unable to delete. Utilisateur with id {} is not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to delete. Utilisateur with id " + id + " is not found."),
					HttpStatus.NOT_FOUND);
		}
		utilisateurService.deleteUtilisateurById(id);
		return new ResponseEntity<Utilisateur>(HttpStatus.NO_CONTENT);
	}

	// ------------------- Supprimer Tous Utilisateurs-----------------------------
	@RequestMapping(value = "/utilisateur", method = RequestMethod.DELETE)
	public ResponseEntity<Utilisateur> deleteAllUtilisateurs() {
		logger.info("Deleting All Utilisateurs");

		utilisateurService.deleteAllUtilisateurs();
		return new ResponseEntity<Utilisateur>(HttpStatus.NO_CONTENT);
	}

}