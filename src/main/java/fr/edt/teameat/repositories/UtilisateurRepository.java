package fr.edt.teameat.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import fr.edt.teameat.model.Utilisateur;

@Transactional

public interface UtilisateurRepository extends CrudRepository<Utilisateur, Long>{

	@Override
	public List<Utilisateur> findAll();

	Utilisateur findByNom(String nom);

	Utilisateur findOneByMailAndMotdepasse(String mail, String motdepasse);

	Utilisateur findOneByMail(String mail);

}
