package fr.edt.teameat.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import fr.edt.teameat.model.Restaurant;

@Transactional

public interface RestaurantRepository extends CrudRepository<Restaurant, Long> {

	@Override
	public List<Restaurant> findAll();

	Restaurant findByNom(String nom);

}