package fr.edt.teameat.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import fr.edt.teameat.model.Commande;

@Transactional

public interface CommandeRepository extends CrudRepository<Commande, Long> {

	@Override
	public List<Commande> findAll();

	public Commande findByNom(String nom);

	public List<Commande> findByIdParent(Long idParent);

}
