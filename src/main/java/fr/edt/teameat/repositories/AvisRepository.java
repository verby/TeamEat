package fr.edt.teameat.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import fr.edt.teameat.model.Avis;
import fr.edt.teameat.model.Menu;

@Transactional

public interface AvisRepository extends CrudRepository<Avis, Long> {

	@Override
	public List<Avis> findAll();


	List<Avis> findByIdRestaurant(Long id);

}
