package fr.edt.teameat.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import fr.edt.teameat.model.Menu;

@Transactional

public interface MenuRepository extends CrudRepository<Menu, Long> {

	@Override
	public List<Menu> findAll();

	Menu findByNom(String nom);

	List<Menu> findByIdRestaurant(Long id);

}
