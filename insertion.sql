--
-- Utilisateurs
--
INSERT INTO `utilisateur`(`nom`, `prenom`, `mail`, `motdepasse`, `vehicule`)
VALUES 	('Iureva','Nadezhda','niureva@edt.fr','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',false),
		('Blanchetière','Marie','mblanchetiere@edt.fr','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',false),
		('Dodge','Nathan','ndodge@edt.fr','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',true),
		('Meridjen','Nicolas','nmeridjen@edt.fr','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',true),
		('Boisselet','Pierre','pboisselet@edt.fr','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',true),
		('Dubreuil','Christophe','cdubreuil@edt.fr','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',true),
		('Grimault','Antoine','agrimault@edt.fr','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',true),
		('Guest','test','test@test.fr','9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08',false);;

--
-- Restaurants
--
INSERT INTO `restaurant`(`nom`, `adresse`, `telephone`, `horaires`, `statut`)
VALUES 	('Les 3 Brasseurs','56 Avenue du 8 Mai 1945, 38130 Échirolles','04 76 29 17 53','1,2,3,4,5,6,7','NONVALIDE'),
		('McDonalds','Carrefour Grenoble Meylan, 1 Boulevard des Alpes, 38240 Meylan','04 76 41 81 38','1,2,3,4,5,6,7','NONVALIDE'),
		('Gastronomia','2 Rue Marius Charles, 38420 Domène','04 76 77 31 83','1,4,5,6,7','VALIDE'),
		('Au petit creux','33 Rue Jean Jaurès, 38420 Domène','04 76 77 34 10','2,3,4,5,6,7','VALIDE');
--
-- Menus
--
INSERT INTO `menu`(`idRestaurant`,`nom`, `prix`, `description`,`image`)
VALUES 	(4,'tacos standard',5.50,'Un tacos',''),
		(4,'tacos + boisson + frites',8.00,'tacos + boisson + frites',''),
		(4,'tacos double',7.50,'Double tacos',''),
		(3,'Tyrolienne',8.00,'Composants : Provolone, speck, crème fraîche, mozza',''),
		(3,'Reine',8.00,'Composants : Tomate, mozza, jambon, champignons',''),
		(3,'4 fromages',8.00,'Composants : Tomate, provolone, comté, talleggio, gorgonzola, mozzarella',''),
		(3,'4 saisons',8.00,'Composants : Tomate, mozza, jambon, champignons, câpres, artichauts, anchois, olives',''),
		(3,'Gambas',8.00,'Composants : Tomates, mozza, gambas, champignons, crème',''),
		(3,'Reine des mers',8.00,'Composants : Tomate, mozzarella, fruits de mer',''),
		(3,'Alpage',8.00,'Composants : Provolone, bresola, crème, mozzarella',''),
		(3,'Oriantale',8.00,'Composants : Tomate, mozza, merguez, poivrons',''),
		(3,'Jambon Cru',8.00,'Composants : Tomate, jambon cru, tome du Piémont, mozza',''),
		(3,'Tyrolienne',11.40,'Composants : Provolone,speck, crème fraîche, mozza',''),
		(3,'Reine',11.40,'Composants : Tomate, mozza, jambon, champignons',''),
		(3,'4 fromages',11.40,'Composants : Tomate, provolone, comté, talleggio, gorgonzola, mozzarella',''),
		(3,'4 saisons',11.40,'Composants : Tomate, mozza, jambon, champignons, câpres, artichauts, anchois, olives',''),
		(3,'Gambas',11.40,'Composants : Tomates, mozza, gambas, champignons, crème',''),
		(3,'Reine des mers',11.40,'Composants : Tomate, mozzarella, fruits de mer',''),
		(3,'Alpage',11.40,'Composants : Provolone, bresola, crème, mozzarella',''),
		(3,'Oriantale',11.40,'Composants : Tomate, mozza, merguez, poivrons',''),
		(1,'Hambourger Traditionnel',13.40,'Composants : beuf, jambon cru, salade, mozza',''),
		(1,'Saint Marie',14.00,'Composants : tomate, jambon cru, oeuf, salade',''),
		(1,'Fish',11.40,'Composants : saumon, salade, oignon',''),
		(1,'Ribs',15.40,'Composants : que de la bonne viande!',''),
		(2,'Hot Dog',3.40,'Composants : saucisse, pain, sauce au choix',''),
		(2,'Salade ITALIAN MOZZA',7.40,'Composants : basilic, feuilles de roquettes, tomates',''),
		(2,'Le poulet ranch ketchup',5.20,'Composants : poulet, cheddar fondu, oignons, cornichons, salade',''),
		(2,'LE McFLURRY',3.20,'Un délicieux tourbillon glacé','');


--
-- Avis
--
INSERT INTO `avis`(`idRestaurant`, `idUtilisateur`, `dateAvis`, `commentaire`)
VALUES 	(1,2,'2017-03-04 12:23:00','Très bon resto!'),
		(1,3,'2017-04-13 23:00:01','Je vous le conseille aussi'),
		(2,1,'2017-04-13 23:00:01','ça peut aller'),
		(2,4,'2017-04-13 23:00:01','pas mon préféré'),
		(3,6,'2017-04-13 23:00:01','trop bon'),
		(3,2,'2017-04-13 23:00:01','Ils ont changé leurs menus'),
		(3,7,'2017-04-13 23:00:01','il ferme bientôt :('),
		(4,3,'2017-04-13 23:00:01','Ils mettent bcp de viande!'),
		(4,1,'2017-04-13 23:00:01','on commande là bas le mardi!');

--
-- Commandes
--
INSERT INTO `commande`(`idMenu`, `idUtilisateur`, `idParent`,`conducteur`, `accompagne`, `nom`, `dateCommande`,`quantite`, `statut`,`commentaire`, `paye`)
VALUES
		(1,2,null,true,false,'1', '2017-03-04 12:23:00',1,'Valide','Allez commander !',true),
		(1,1,null,true,false,'2', '2017-03-04 12:23:00',2,'Valide','Gogo',true),
		(1,3,null,true,false,'3', '2017-03-04 12:23:00',1,'Valide','trop faim !',true),
		(1,4,null,true,false,'4', '2017-03-04 12:23:00',1,'Valide','Lets go !',true),
		(21,2,1,true,false,'1', '2017-03-04 12:23:00',1,'Valide','Très bien !',true),
		(22,1,1,true,false,'1', '2017-03-04 12:23:00',1,'Valide','Super',true),
		(23,3,1,true,false,'1', '2017-03-04 12:23:00',1,'Valide','Pas mal !',true),
		(24,4,1,true,false,'1', '2017-03-04 12:23:00',1,'Valide','Ca faisait longtemps !',true),
		(21,5,1,true,false,'1', '2017-03-04 12:23:00',1,'Valide','Bonne idée !',true),
		(22,2,1,true,false,'1', '2017-03-04 12:23:00',1,'Valide','Je suis en réunion à 13h',true),
		(25,1,2,false,true,'2', '2017-01-04 12:13:00',1,'Valide','Good !',false),
		(7,6,3,false,false,'3', '2017-04-15 10:23:00',1,'Non Valide',null,false),
		(2,3,4,false,true,'4', '2017-03-27 17:15:00',1,'Valide','Soyons fous !!!',true);
